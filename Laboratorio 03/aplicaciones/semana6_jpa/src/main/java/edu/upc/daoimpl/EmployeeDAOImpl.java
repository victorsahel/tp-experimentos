package edu.upc.daoimpl;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import edu.upc.dao.IEmployeeDAO;
import edu.upc.entity.Employee;
@Stateless
public class EmployeeDAOImpl implements IEmployeeDAO, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@PersistenceContext(unitName = "semana6PU")
	private EntityManager manager;

	@Override
	public void insertar(Employee t) {
		try {
			manager.persist(t);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

	@Override
	public void actualizar(Employee t) {
		try {
			manager.merge(t);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

	@Override
	public void eliminar(int idT) {
		try {
			Employee emp = new Employee();
			emp = manager.getReference(Employee.class, idT);
			manager.remove(emp);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Employee> listar() {
		List<Employee> lista = null;
		try {
			Query query = manager.createQuery("from Employee e");
			lista = (List<Employee>) query.getResultList();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return lista;	}

	@SuppressWarnings("unchecked")
	@Override
	public Employee listarId(int idT) {
		List<Employee> lista = null;
		Employee emple = new Employee();
		try {
			Query query = manager.createQuery("from Employee e where e.idEmployee=?1");
			query.setParameter(1, idT);
			lista = (List<Employee>) query.getResultList();
			if (!lista.isEmpty() && lista != null) {
				emple = lista.get(0);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return emple;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Employee> buscarNombre(Employee employee) {
		List<Employee> lista = null;
		try {
			Query query = manager.createQuery("from Employee e " 
					+ "where e.nameEmployee like ?1");
			query.setParameter(1, "%" + employee.getNameEmployee() + "%");
			lista = (List<Employee>) query.getResultList();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return lista;
	}

}
